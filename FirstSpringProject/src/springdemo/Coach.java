package springdemo;

public interface Coach {
    String getDailyWorkout();
}
